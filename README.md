# centos-systemd-docker
an centos docker that support systemd


## running
```
docker run -tid -v /sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/hiproz/centos-systemd-docker
```
note:
This container is running with systemd in a limited context, with the cgroups filesystem mounted. There have been reports that if you're using an Ubuntu host, you will need to add -v /tmp/$(mktemp -d):/run in addition to the cgroups mount.
